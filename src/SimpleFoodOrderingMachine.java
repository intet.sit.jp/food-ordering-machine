import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel Total;
    private JButton startOverButton;
    int currentSum;
    int sum;
    int tax;//Tax
    double tax_fee;
    int total_fee;

    void tax(){//Tax Confirm
        JFrame frame = new JFrame();
        int option = JOptionPane.showConfirmDialog(null, "Do you take it home?",
                "Tax confirm", JOptionPane.YES_NO_OPTION);
        if(option==1){
            tax=10;
            tax_fee=1.1;
        }else{
            tax=8;
            tax_fee=1.08;
        }
    }

    void order(String food, int cost){
        int confirmation=JOptionPane.showConfirmDialog(null,"Would you like to order "+ food+" ?","Order Confirmation"
                ,JOptionPane.YES_NO_OPTION );

        if(confirmation ==0) {
            String currentText = textPane1.getText();
            sum = currentSum+cost;
            JOptionPane.showMessageDialog(null, "Order for "+food+" received.","Order received ",JOptionPane.INFORMATION_MESSAGE);
            textPane1.setText(currentText +food +"  "+cost+" yen"+"\n");
            total_fee =(int)(sum * tax_fee);
            Total.setText("Total: "+total_fee+" yen"+"  Tax: "+tax+"%");
            currentSum=sum;
        }
    }

    public SimpleFoodOrderingMachine() {

        tax();//Confirm Tax
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cheese　burger",400);
            }
        });
        button1.setIcon(new ImageIcon(
                this.getClass().getResource("1.jpg")
        ));

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shrimp burger",370);

            }
        });
        button2.setIcon(new ImageIcon(
                this.getClass().getResource("2.jpg")
        ));

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Vegetable burger",470);
            }
        });
        button3.setIcon(new ImageIcon(
                this.getClass().getResource("3.jpg")
        ));

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried Onion Rings",350);

            }
        });
        button4.setIcon(new ImageIcon(
                this.getClass().getResource("4.jpg")
        ));

        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ice Coffee",250);
            }
        });
        button5.setIcon(new ImageIcon(
                this.getClass().getResource("5.jpg")
        ));

        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cup salad",275);
            }
        });
        button6.setIcon(new ImageIcon(
                this.getClass().getResource("6.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,"Would you like to Checkout ?","Checkout Confirmation"
                        ,JOptionPane.YES_NO_OPTION );
                if(confirmation ==0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+total_fee+" yen.","message",JOptionPane.INFORMATION_MESSAGE);
                    sum=0;
                    currentSum=0;
                    Total.setText("Total: "+sum+" yen");
                    textPane1.setText("");
                    tax();//Confirm Tax again
                }
            }
        });
        startOverButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,"Would you like to Start Over ?","Start Over Confirmation"
                        ,JOptionPane.YES_NO_OPTION );
                if(confirmation ==0) {
                    sum = 0;
                    currentSum = 0;
                    Total.setText("Total: " + sum + " yen");
                    textPane1.setText("");
                    tax();//Confirm Tax again
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
